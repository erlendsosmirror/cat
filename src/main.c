/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int CatFile (char *programname, char *filename)
{
	// Allocate on the stack
	unsigned char copybuffer[512];

	// Open source
	int source = open (filename, O_RDONLY);

	if (source < 0)
		return PrintError (programname, filename);

	// Copy
	for (;;)
	{
		// Read up to 512 bytes
		int nread = read (source, copybuffer, 512);

		// Check for zero (end of file) and errors
		if (nread == 0)
			break;
		else if (nread < 0)
		{
			close (source);
			return PrintError (programname, filename);
		}

		// Write nread bytes
		int n = nread;
		int pos = 0;

		do
		{
			int diff = write (1, copybuffer + pos, n);

			// Check for errors
			if (diff < 0)
			{
				close (source);
				return PrintError (programname, filename);
			}

			pos += diff;
			n -= diff;
		} while (pos != nread);
	}

	// Done
	close (source);
	return 0;
}

int main (int argc, char **argv)
{
	if (argc <= 1)
	{
		SimplePrint ("Usage: cat <files>\n");
		return 0;
	}

	for (int i = 1; i < argc; i++)
	{
		int ret = CatFile (argv[0], argv[i]);

		if (ret)
			return ret;
	}

	return 0;
}
